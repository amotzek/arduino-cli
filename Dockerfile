FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
&& apt-get install -y curl git build-essential ffmpeg python3 python3-pip \
&& apt-get clean \
&& ln /usr/bin/python3 /usr/bin/python

RUN pip3 install pyserial

RUN curl -O https://downloads.arduino.cc/arduino-cli/arduino-cli_0.13.0_Linux_64bit.tar.gz \
&& tar xvzf arduino-cli_0.13.0_Linux_64bit.tar.gz \
&& rm arduino-cli_0.13.0_Linux_64bit.tar.gz \
&& mv arduino-cli /usr/bin

RUN arduino-cli core install esp32:esp32 --additional-urls https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

RUN curl -O https://raw.githubusercontent.com/espressif/esp-idf/8bc19ba893e5544d571a753d82b44a84799b94b1/components/spiffs/spiffsgen.py

RUN git clone https://github.com/othercrashoverride/odroid-go-firmware.git -b factory \
&& cd odroid-go-firmware/tools/mkfw \
&& make \
&& mv mkfw /usr/bin \
&& cd / \
&& rm -rf odroid-go-firmware
